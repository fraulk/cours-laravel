@extends('layouts.layout')

@section('title')
AAAAAAAAAAAAAAHHHH
@endsection

@section('content')
    <h1>Articles</h1>
    <a href="{{route('postAdd')}}" class="btn btn-primary">Ajouter un article</a>
    @if (sizeof($posts) > 0)
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-4">
                    <div class="card">
                        <img src="{{asset("storage/$post->picture")}}" alt="" class="card-img-top" style="object-fit: cover; height: 200px;">
                        <div class="card-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <p>{{$post->extrait}}</p>
                            <p>Il y a {{$post->countComments()}} commentaires</p>
                            <div>
                                @foreach ($post->categories as $category)
                                    <span>{{$category->name}}</span>
                                @endforeach
                            </div>
                            <div class="d-flex">
                                <a href="{{route("postDetail", $post->id)}}" class="btn btn-primary">Details</a>
                                <form action="{{route('postDelete', $post->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">Supprimer</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
    <p>Pas d'articles</p>
    @endif
    {{-- <ul>
    @foreach ($posts as $item)
        <li>
            <div><a href="{{route('postDetail', $item->id)}}">{{$item->title}}</a></div>
            <div>{{$item->description}}</div>
        </li>
    @endforeach
    </ul> --}}
@endsection