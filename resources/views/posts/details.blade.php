@extends('layouts.layout')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-6">
            <form action="{{route('postUpdate', $post->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <label>Titre</label>
                    <input type="text" class="form-control" name="title" required value="{{$post->title}}">
                </div>
                <div class="form-group">
                    <label>Extrait</label>
                    <input type="text" class="form-control" name="extrait" required value="{{$post->extrait}}">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea name="description" class="form-control" rows="5" required>{{$post->description}}</textarea>
                </div>
                <div>
                @foreach ($categories as $category)
                    <div class="form-check form-check-inline">
                        <input type="checkbox" value="{{$category->id}}" id="check-{{$category->id}}" class="form-check-input"
                            name="checkboxCategories[{{$category->id}}]"    
                            @if($post->categories->contains('id', $category->id)) checked @endif>
                        <label for="check-{{$category->id}}" class="form-check-label">{{$category->name}}</label>
                    </div>    
                @endforeach
                </div>
                <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
            <p>Ecrit le {{$post->created_at->format('d/m/Y')}}</p>
        </div>
        <div class="col-md-6">
            <form action="{{route('postUpdatePicture', $post->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>Modifier l'image</label>
                    <input type="file" name="picture" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
        </div>
    </div>

    {{-- <h1>{{$post->title}}</h1>
    <p>{{$post->description}}</p> --}}
    <a href="{{route('postList')}}"
     class="btn btn-primary"
    >Retour à la liste des articles</a>
    <form action="{{route('postDelete', $post->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger">Supprimer</button>
    </form>

    <h2>Commentaires</h2>
    @if (sizeof($post->comments) > 0)
        <ul>
            @foreach ($post->comments as $comment)
                <li>
                    <p>{{$comment->content}}</p>
                    <form action="{{route('commentDelete', $comment->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm">Supprimer</button>
                    </form>
                </li>
            @endforeach
        </ul>
    @else
        <p>Pas de commentaires</p>
    @endif

    <form action="{{route('commentAdd', $post->id)}}" method="post">
        @csrf
        <div class="form-group">
            <label>Votre commentaire</label>
            <input type="text" name="content" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Ajouter commentaire</button>
    </form>

</div>
@endsection