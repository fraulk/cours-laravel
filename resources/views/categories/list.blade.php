@extends('layouts.layout')

@section('content')
    <div class="container">
        <h1>Les categories</h1>
        <ul>
            @foreach ($categories as $category)
                <li>
                    <form action="{{route('categoryUpdate', $category->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" value="{{$category->name}}" required>
                        </div>
                        <button type="submit" class="btn btn-warning btn-sm">Modifier</button>
                    </form>
                    <form action="{{route('categoryDelete', $category->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm">Supprimer</button>
                    </form>
                </li>
            @endforeach
        </ul>
        <a href="{{route('categoryAdd')}}" class="btn btn-primary">Ajouter une categorie</a>
    </div>
@endsection