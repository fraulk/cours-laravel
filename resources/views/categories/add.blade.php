@extends('layouts.layout')

@section('content')
    <div class="container">
        <h1>Ajouter une categorie</h1>
        <form action="{{route('categoryStore')}}" method="post">
            @csrf
            <div class="form-group">
                <label>Nom de la categorie</label>
                <input type="text" name="name" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </form>
    </div>
@endsection