<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdatePictureRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function test()
    {
        $loading = false;
        $posts = Post::orderBy('updated_at', 'DESC')->get();
        // $posts = Post::all();
        // $posts = DB::table('posts')->get();
        // dd($posts);
        return view("posts.list", compact(['loading', 'posts']));
        // return view("test")
        //     ->with('loading', $loading)
        //     ->with('loading', $loading)
        //     ->with('loading', $loading);
    }

    public function details($id)
    {
        $post = Post::find($id);
        $categories = Category::all();
        // $post = Post::where('title', 'azaeaeaz')->first();
        // dd($post);
        return view('posts.details', compact(['post', 'categories']));
    }

    public function add()
    {
        $categories = Category::all();
        return view("posts.add", compact('categories'));
    }

    public function store(PostStoreRequest $request)
    {
        $params = $request->validated();
        $file = Storage::put('public', $params['picture']);
        $params['picture'] = substr($file, 7);
        $post = Post::create($params);
        if (!empty($params['checkboxCategories'])) {
            $post->categories()->attach($params['checkboxCategories']);
        }
        return redirect()->route("postList");
        // Post::create([
        //     'title' => $params['title'],
        //     'description' => $params['description'],
        // ]);
    }

    public function update($id, PostUpdateRequest $request)
    {
        $params = $request->validated();
        $post = Post::find($id);
        $post->update($params);
        $post->categories()->detach();
        if (!empty($params['checkboxCategories'])) {
            $post->categories()->attach($params['checkboxCategories']);
        }
        return redirect()->route('postDetail', $id);
    }

    public function updatePicture($id, PostUpdatePictureRequest $request)
    {
        $params = $request->validated();
        $post = Post::find($id);
        if (Storage::exists("public/$post->picture")) {
            Storage::delete("public/$post->picture");
        }
        $file = Storage::put('public', $params['picture']);
        $params['picture'] = substr($file, 7);
        $post->picture = $params['picture'];
        $post->save();
        return redirect()->route('postDetail', $id);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        if (Storage::exists("public/$post->picture")) {
            Storage::delete("public/$post->picture");
        }
        $post->delete();
        return redirect()->route("postList");
    }
}
