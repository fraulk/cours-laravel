<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test/{name?}', function (\Illuminate\Http\Request $request, $name = null) {
//     dd($request);
// });

Route::get("/", [PostController::class, "test"])->name("postList");
Route::get("/articles/ajouter", [PostController::class, "add"])->name("postAdd");
Route::post("/articles/ajouter", [PostController::class, "store"])->name("postStore");
Route::get("/articles/{id}", [PostController::class, "details"])->name("postDetail");
Route::post("/articles/{id}/modifier", [PostController::class, "update"])->name("postUpdate");
Route::put("/articles/{id}/modifier/image", [PostController::class, "updatePicture"])->name("postUpdatePicture");
Route::delete("/articles/{id}/supprimer", [PostController::class, "delete"])->name("postDelete");

Route::post('/commentaires/{postId}', [CommentController::class, 'store'])->name('commentAdd');
Route::delete('/commentaires/{id}', [CommentController::class, 'delete'])->name('commentDelete');

Route::get('categories', [CategoryController::class, "index"])->name('categoryList');
Route::get('categories/ajouter', [CategoryController::class, "add"])->name('categoryAdd');
Route::post('categories/ajouter', [CategoryController::class, "store"])->name('categoryStore');
Route::put('category/{id}/modifier', [CategoryController::class, "update"])->name("categoryUpdate");
Route::delete('category/{id}/supprimer', [CategoryController::class, "delete"])->name("categoryDelete");


require __DIR__ . '/auth.php';
